#!/usr/bin/make -f

export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS := hardening=+all
export DH_ALWAYS_EXCLUDE := COPYING:LICENSE
export LANG:=C

include /usr/share/dpkg/pkg-info.mk

# Get Debian package version
DPKG_QUERY_OPTS= --show --showformat='$${version}'
DEB_PKG_VERSION=$(call dpkg_late_eval,$1, dpkg-query ${DPKG_QUERY_OPTS} $2)

# Set CGE version
CGE_VER=$(call DEB_PKG_VERSION,CGE_VER,fp-units-castle-game-engine)

# Get directories
CURDIR:=$(shell pwd)
TMP_DIR=$(CURDIR)/debian/tmp

# Set default compilation options
BUILDOPTS=\
	--compiler-option=@castle-game-engine \
	--compiler-option=@hardening \

%:
	dh ${@}

override_dh_auto_clean:
	dh_auto_clean -- -i
	$(MAKE) clean-code

override_dh_auto_build:override_dh_auto_build-arch override_dh_auto_build-indep
override_dh_auto_build-arch:
	mkdir -p $(TMP_DIR)

	# Once pasdoc ships file_to_pascal_string, the next lines can be simplified
	fpc -FE$(TMP_DIR) @hardening debian/file_to_pascal_string.dpr
	PATH=${PATH}:$(TMP_DIR) $(MAKE) generate-code

	${MAKE} compile CASTLE_ENGINE_TOOL_OPTIONS='$(BUILDOPTS)'
	inkscape --export-width=48 --export-height=48 freedesktop/castle-model-viewer.svg \
		--export-filename=freedesktop/castle-model-viewer

override_dh_auto_build-indep:

override_dh_auto_test:
	# Code to run the package test suite.
	# We need the directories where the files to test are to be
	# writeable by the current (non-root) user. Copying them in
	# the current tree is the easiest way to accomplish that.
	#
	# TBD. New tests planned using castle-game-engine

override_dh_auto_install-arch:
	fpc-depends
	echo CGE:Version=${CGE_VER} >> debian/castle-model-viewer.substvars

override_dh_auto_install-indep:

lintian:
	lintian -I -i ../${DEB_SOURCE}_${DEB_VERSION}.dsc
