Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: view3dscene
Upstream-Contact: Michalis Kamburelis <kambi@users.sf.net>
Source: https://castle-engine.io/view3dscene.php

Files: *
Copyright: 1999-2017 Michalis Kamburelis <kambi@users.sf.net>
 2008-2017 Katarzyna Obrycka
License: GPL-2+
Comment: regarding macosx/template.dmg.bz2:
 - it's all mine
 - but the program to actually edit this data ("Finder") is
   proprietary, and only on Mac OS X, and I don't think there is any
   alternative to it.

Files: embedded_data/images/warning_icon.svg
Copyright: 2008 Amada44
Comment: https://commons.wikimedia.org/wiki/File:Warning_icon.svg
License: public-domain
 This work has been released into the public domain by its author, Amada44.
 This applies worldwide.
 .
 In some countries this may not be legally possible; if so:
 .
 Amada44 grants anyone the right to use this work for any purpose, without any
 conditions, unless such conditions are required by law.

Files: embedded_data/images/open.png
Copyright: Tango Desktop Project
Comment: color modified by Kambi (using GIMP)
 .
 http://tango.freedesktop.org/Tango_Desktop_Project
License: public-domain
 The Tango base icon theme is released to the Public Domain. The palette
 is in public domain. Developers, feel free to ship it along with your
 application. The icon naming utilities are licensed under the GPL.
 .
 Though the tango-icon-theme package is released to the Public Domain,
  we ask that you still please attribute the Tango Desktop Project, for
 all the hard work we've done. Thanks. 

Files: embedded_data/images/walk.svg
Copyright: 2006 ryanlerch
 2010 Michalis Kamburelis <kambi@users.sf.net>
Comment:
 Original File ryanlerch_No_entry_sign_with_a_man.svg
 modified heavily by Kambi (who takes responsibility for all the ugliness
 caused by his editing :) (using Inkscape)
License: public-domain
 Govermental road sign

Files: embedded_data/images/examine.svg
Copyright: 2007 BenFrantzDale
 2007 Fibonacci
 2010 Michalis Kamburelis <kambi@users.sf.net>
Comment: modified by Kambi (using Inkscape)
 .
 https://commons.wikimedia.org/wiki/File:Necker_cube.svg
License: GFDL-1.2+

Files: embedded_data/images/fly.svg
Copyright: 2007 Liftarn
 2010 Michalis Kamburelis <kambi@users.sf.net>
Comment: https://commons.wikimedia.org/wiki/File:Falco-peregrinus-silhouette.svg
License: GFDL-1.2+

Files: example_models/water/shaders/noise3Dgrad.glsl
Copyright: 2011 Ashima Arts
License: Expat

Files: debian/*
Copyright: 2013-2025 Abou Al Montacir <abou.almontacir@sfr.fr>
 2013-2020 Paul Gevers <elbrus@debian.org>
 2023-2025 Peter Blackman <peter@pblackman.plus.com>
License: GPL-2+

Files: debian/file_to_pascal_string.dpr
Copyright: 1998-2014 PasDoc developers
Comment: copyied from the pasdoc package until pasdoc ships the binary
 pasdoc/source/tools/file_to_pascal_string.dpr
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems a copyt of the GPL version 2 can be found at
 /usr/share/common-licenses/GPL-2

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2 or
 any later version published by the Free Software Foundation; with no
 Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 .
 On Debian systems a copy of the GFDL version 1.3 can be found at
 /usr/share/common-licenses/GFDL-1.3
 .
 .
 This file is licensed under the Creative Commons Attribution-Share Alike
 3.0 Unported license.
 .
 You are free:
 .
   to share – to copy, distribute and transmit the work
   to remix – to adapt the work
 .
 Under the following conditions:
 .
   attribution – You must attribute the work in the manner specified by
                 the author or licensor (but not in any way that suggests
                 that they endorse you or your use of the work).
   share alike – If you alter, transform, or build upon this work, you
                 may distribute the resulting work only under the same or
                 similar license to this one.
 .
 .
 This file is licensed under the Creative Commons Attribution-Share Alike
 2.5 Generic, 2.0 Generic and 1.0 Generic license.
 .
 You are free:
 .
   to share – to copy, distribute and transmit the work
   to remix – to adapt the work
 .
 Under the following conditions:
 .
   attribution – You must attribute the work in the manner specified by
                 the author or licensor (but not in any way that suggests
                 that they endorse you or your use of the work).
   share alike – If you alter, transform, or build upon this work, you
                 may distribute the resulting work only under the same or
                 similar license to this one.

License: Expat
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
  .
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
  .
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
  
